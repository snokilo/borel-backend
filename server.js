const http = require('http');
const app = require('./app');
require('dotenv').load();

const port = process.env.PORT || 3000;

const server = http.createServer(app);

console.log('The web server started on port ' + port);

server.listen(port);