const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const cors = require("cors");
var fs = require('fs');
require('dotenv').load();


const productRoutes = require("./api/routes/products");
const orderRoutes = require("./api/routes/orders");
const userRoutes = require('./api/routes/user');

mongoose.connect(
  'mongodb://node-cafe-etudiant:' + process.env.MONGO_ATLAS_PW + '@node-cafe-etudiant-shard-00-00-3c2br.mongodb.net:27017,node-cafe-etudiant-shard-00-01-3c2br.mongodb.net:27017,node-cafe-etudiant-shard-00-02-3c2br.mongodb.net:27017/test?ssl=true&replicaSet=node-cafe-etudiant-shard-0&authSource=admin&retryWrites=true',
  {
    useMongoClient: true
  },
);
mongoose.Promise = global.Promise;

app.use('/uploads', express.static('uploads'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  if (req.method === "OPTIONS") {
    res.header("Access-Control-Allow-Methods", "PUT, POST, PATCH, DELETE, GET");
    return res.status(200).json({});
  }
  next();
});

try{
	fs.mkdirSync('uploads');
	console.log('Folder created');
} catch(err){
	if(err.code === 'EEXIST')
	{
		console.log('Folder already exist');
	} else{
	console.log(err);
}}

// Routes which should handle requests
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);
app.use("/user", userRoutes);

app.use((req, res, next) => {
  const error = new Error("Cette route n'existe pas");
  error.status = 404;
  next(error);
});

app.use((error, req, res, next) => {
  res.status(error.status || 500);
  res.json({
    error: {
      message: error.message
    }
  });
});

module.exports = app;
