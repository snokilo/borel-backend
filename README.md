# Borel Backend server, this is the api for the Borel system

## Usage
* Check out the branch you're interested in (i.e. which belongs to the video in my series you just watched), ```git clone``` it and thereafter run ```npm install```.

* You now have to add a .env witch specifies (add all the env variables here)

* Swith to the develop branch
``` git checkout develop ```

* Make your own branch with one of the naming template
    * ` bugfix/(Name of the bug you are fixing here) ` these branches are used to fix non-urgent bug correction
    * ` feature/(Name of the feature you are doing here) ` these branches are used to create feature
    * ` hotfix/(Name of the urgent bug you are fixing here ` these branches are user to fix urgent bug on production

* Make your changes

* Make a pull request to the develop branch

* Merge the develop branch to the staging branch when all code for an update is ready
* Merge the staging branch to the production branch if all the test passed and erverything is fine.
 