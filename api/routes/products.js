const express = require("express");
const router = express.Router();
const multer = require('multer');
const checkAuth = require('../middleware/check-auth');
const ProductsController = require('../controllers/products');

/**
 * Module storing all products function
 * @module products
 */

/**
 * Define the folder to store the product image
 */
const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, './uploads');
  },
  filename: function(req, file, cb) {
    cb(null, Date.now() + '-' + file.fieldname + '.png');
  }
});

  /**
   * Filter the file passed in the body of the request
   */
  fileFilter = (req, file, cb) => {
  // reject a file
  if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

/**
 * Upload the file in the folder
 */
const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 5
  },
  fileFilter: fileFilter
});

/**
 * Handle incoming GET requests to /products
 */
router.get("/", ProductsController.products_get_all);

/**
 * Handle incoming POST requests to /products
 */
router.post("/", checkAuth, upload.single('productImage'), ProductsController.products_create_product);

/**
 * Handle incoming GET requests to /'productId here'
 */
router.get("/:productId", ProductsController.products_get_product);

/**
 * Handle incoming PATCH requests to /'productId here'
 */
router.patch("/:productId", checkAuth, ProductsController.products_update_product);

/**
 * Handle incoming DELETE requests to /'productId here'
 */
router.delete("/:productId", checkAuth, ProductsController.products_delete);

module.exports = router;
