const express = require("express");
const router = express.Router();
const UserController = require('../controllers/user');
const checkAuth = require('../middleware/check-auth');

/**
 * Module storing all products function
 * @module users
 */

/**
 * Handle POST requests to /signup
 */
router.post("/signup", UserController.user_signup);

/**
 * Handle POST requests to /login
 */
router.post("/login", UserController.user_login);

/**
 * Handle DELETE requests to /'userId here'
 */
router.delete("/:userId", checkAuth, UserController.user_delete);

module.exports = router;
