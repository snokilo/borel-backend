const mongoose = require('mongoose');

/**
 * Module storing all products function
 * @module products
 */

/**
 * Mongoose model for proucts
 */
const productSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: { type: String, required: true },
    price: { type: Number, required: true },
    productImage: { type: String, required: false },
    quantity: {type: String, required: false },
});

module.exports = mongoose.model('Product', productSchema);