const jwt = require('jsonwebtoken');
require('dotenv').load();

/**
 * Module storing all auths function
 * @module auths
 */

/**
 * Check if a user token is valid or not
 */
module.exports = (req, res, next) => {
    try {
        const token = req.headers.authorization.split(" ")[1];
        const decoded = jwt.verify(token, process.env.JWT_KEY);
        req.userData = decoded;
        next();
    } catch (error) {
        return res.status(401).json({
            message: 'Auth failed'
        });
    }
};