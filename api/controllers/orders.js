const mongoose = require("mongoose");
const jwt = require('jsonwebtoken');
require('dotenv').load();

const Order = require("../models/order");
const Product = require("../models/product");
const User = require("../models/user");

/**
 * Module storing all orders function
 * @module orders
 */

/**
 * Return in a JSON all of the orders stored in the database
 */
exports.orders_get_all = (req, res, next) => {
  Order.find()
    .select("product quantity _id")
    .populate("product", "name")
    .exec()
    .then(docs => {
      res.status(200).json({
        count: docs.length,
        orders: docs.map(doc => {
          return {
            _id: doc._id,
            product: doc.product,
            quantity: doc.quantity,
            request: {
              type: "GET",
              url: process.env.LINK_BASE + ":" + process.env.PORT + "/orders/" + doc._id
            }
          };
        })
      });
    })
    .catch(err => {
      res.status(500).json({
        error: err
      });
    });
};

/**
 * Create an order in the database 
 */
exports.orders_create_order = (req, res, next) => {
  console.log("lol");
  const token = req.headers.authorization.split(" ")[1]
  Product.findById(req.body.productId)
    .then(product => {
      if (product) {
        const decoded = jwt.verify(token, process.env.JWT_KEY);
        if(req.body.quantity <= product._doc.quantity){
          const order = new Order({
            _id: mongoose.Types.ObjectId(),
            quantity: req.body.quantity,
            product: req.body.productId,
            user: decoded.userId,
          });
          //change the quantity of the product in the database
          var newQuantity = product._doc.quantity -= req.body.quantity;
          Product.findOneAndUpdate({ _id: req.body.productId }, { $set: { quantity: newQuantity }}, {new: true } ,function(error,result){});
          
          return order.save();  
        } else {
          return res.status(409).json({
            message: "The amount ordered isn't available in stock, there is only " + product._doc.quantity + " left"
          });
        }
      } else {
        return res.status(404).json({
          message: "Product not found",
        });
      }
    })
    .then(result => {
      res.status(201).json({
        message: "Order stored",
        createdOrder: {
          _id: result._id,
          product: result.product,
          quantity: result.quantity,
          user: result.user
        },
        request: {
          type: "GET",
          url: process.env.LINK_BASE + ":" + process.env.PORT + "/orders/" + result._id
        }
      });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};
/**
 * Get a precise order from the database
 */
exports.orders_get_order = (req, res, next) => {
  Order.findById(req.params.orderId)
    .populate("product")
    .populate("user")
    .exec()
    .then(order => {
      if (!order) {
        return res.status(404).json({
          message: "Order not found"
        });
      }
      res.status(200).json({
        order: order
      });
    })
    .catch(err => {
      res.status(500).json({
        error: err
      });
    });
};
/**
 * Delete an order from the database
 */
exports.orders_delete_order = (req, res, next) => {
  Order.remove({ _id: req.params.orderId })
    .exec()
    .then(result => {
      res.status(200).json({
        message: "Order deleted",
      });
    })
    .catch(err => {
      res.status(500).json({
        error: err
      });
    });
};
